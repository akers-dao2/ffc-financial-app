##The FFC Financial Application 

----------

The FFC Financial Application was a software program that facilitated the management of tithes and offerings for a local church. 

**Installation**:

You need node.js and npm. At the root of the project type:

```node
NPM install
```

**Run Project:**

```node
npm run start
```

**Software Used:**

1. CSS3 (BootStrap http://getbootstrap.com) 
1. HTML5
1. Angularjs (https://angularjs.org) use for MVVM
1. HighCharts.js (http://www.highcharts.com) used for Charts visualization 
1. Parse.js (https://parse.com) used for Database
1. Select.js (http://github.hubspot.com/select/docs/welcome/) used to enhance select element
1. Yeoman (http://yeoman.io) workflow tooling for kickstarting new projects

----------
**Example:**

- The Dashboard view provides a graphical representation of the tithes and offerings data based on months or year view
- Dashboard view also provides the raw data behind the graphical view as well as provide statistical information.  
- The Contributions modal screen allows for entering tithes and offerings data

![FFC_Financial](FFC_Financial.gif)