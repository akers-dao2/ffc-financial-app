var ffc = angular.module('ffc', ['ui', 'ffc.directives', 'ffc.service', 'ngResource']);

ffc.controller('mainCTRL', function ($scope, $http, $filter, parse, contribution) {
    /**
     * Represents a book.
     * @constructor
     * @param {string} data - The title of the book.
     * @param {string} year - The author of the book.
     */

    var GroupData = (function () {
        function GroupData(data, year) {
            /**
             * array of objects with propery of date and sum
             * @type {array}
             */
            this.dataSum = [];
            /**
             * the current date.
             * @type {date}
             */
            var curDate = new Date();
            /**
             * the year provided or current year.
             * @type {string}
             */
            this.year = year || '2014';
            /**
             * Splits a collection into sets, grouped by the result of running each value through iteratee
             * @param {project.MyClass} obj Instance of MyClass which leads to a long
             *     comment that needs to be wrapped to two lines.
             * @return {object} Whether something occurred.
             */
            var group = _.groupBy(data, function (num) {
                var d = new Date(num.date);
                return d.getMonth() + 1 + "" + d.getFullYear();
            });
            var that = this;
            /**
             * Looks through each value in the list, returning an array of all the values that pass a truth test
             *@return {array} Whether something occurred. 
             */
            var filter = _.filter(group, function (num, key, list) {
                return key.toString().substr(key.toString().length - 4) === that.year;
            });

            _.each(filter, function (value, list) {
                var obj = {};
                var d = new Date(value[0].date);
                var month = $scope.months[d.getMonth()];

                var sum = _.reduce(value, function (memo, num) {
                    return memo + num.amount;
                }, 0);

                obj.date = month;
                obj.amount = sum;
                that.dataSum.push(obj);

            });
        }

        GroupData.prototype.getData = function () {
            return this.dataSum;
        };
        return GroupData;
    })();

    var Contribution = new parse();
    Contribution.getData(200, {
        success: function (results) {
            var data = contribution(results);
            var groupData = new GroupData(data);
            $scope.$apply(function () {
                $scope.datas = groupData.getData();
                new AdditionalInfo(groupData.getData());
            })

        }
    });

    //set month variable on the $scope object displaying Month on chart screen
    $scope.monthName = "Months";
    //set year variable on the $scope object displaying Year on the chart screen
    $scope.yearName = "Years";
    //set months array on the $scope object displaying Months on the dropdown windw
    $scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    //set years array on the $scope object displaying Years on the dropdown windw
    $scope.years = [2011, 2012, 2013, 2014, 2015];
    //set change function on the $scope object.  Handles change the month and year variables
    //based on what is selected from the dropdown box
    $scope.change = function (value, index) {
        var arr;
        if (typeof value === 'number') {
            $scope.yearName = value;
            $scope.monthName = 'All';
            Contribution.getData(200, {
                success: function (results) {
                    var data = contribution(results);
                    var groupData = new GroupData(data, value.toString());
                    arr = groupData.getData();
                    $scope.datas = arr;
                    $scope.$apply(function () {
                        new AdditionalInfo(arr);
                    });
                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }

            });
            $scope.showHide = false;
        } else {
            $scope.monthName = value;
            $scope.showHide = true;
            Contribution.getData(200, {
                success: function (results) {
                    var data = contribution(results);

                    if (index < 12) {
                        arr = _.filter(data, function (data) {
                            var d = new Date(data.date);
                            var curDate = new Date();
                            if ($scope.yearName === "Years") {
                                return d.getMonth() + "" + d.getFullYear() === index + "" + curDate.getFullYear();
                            } else {
                                return d.getMonth() + "" + d.getFullYear() === index + "" + $scope.yearName;
                            }
                        });
                    } else {
                        var groupData = new GroupData(data, $scope.yearName.toString());
                        arr = groupData.getData();
                    }
                    $scope.$apply(function () {
                        $scope.datas = arr;
                        new AdditionalInfo(arr);
                    });


                },
                error: function (error) {
                    alert("Error: " + error.code + " " + error.message);
                }
            });

        }

    };
    var AdditionalInfo = (function () {
        function AdditionalInfo(arr) {
            //max amount from the data
            var max = _.max(arr, function (d) {
                return d.amount;
            });
            $scope.highest = max.amount;

            //min amount from the data
            var min = _.min(arr, function (d) {
                return d.amount;
            });
            $scope.lowest = min.amount;

            //sum amount of the data
            var sum = _.reduce(arr, function (memo, num) {
                return memo + num.amount;
            }, 0);

            //average
            $scope.average = sum / arr.length;


        }

        return AdditionalInfo
    })()
    //set delete function on the $scope object. delete a give object from the table list
    $scope.delete = function (index) {
        var loop = 0;
        var arr = _.filter($scope.datas, function (data) {
            if (index != loop) {
                loop++;
                return data;
            }
            loop++;
            var query = new Parse.Query(Contribution.getClassName());
            query.get(data.id, {
                success: function (contribution) {
                    contribution.destroy({
                        success: function (myObject) {
                            // The object was deleted from the Parse Cloud.
                            console.log("deleted: " + myObject)
                        },
                        error: function (myObject, error) {
                            // The delete failed.
                            // error is a Parse.Error with an error code and description.
                            console.log("Error: " + error)
                        }
                    });
                },
                error: function (object, error) {
                    // The object was not retrieved successfully.
                    // error is a Parse.Error with an error code and description.
                    console.log("Error: " + error)
                }
            });
        });
        $scope.datas = arr;
    };

    //set current date on modal.html
    //$scope.date = new Date();
    //set save function on the $scope object. saves a give object enter in via the contribution form
    $scope.save = function (d, a, t) {
        var data = {
            //            id: $scope.datas.length,
            date: $filter('date')(d, 'yyyy-MM-dd'),
            amount: a,
            type: t
        };
        var temp = $scope.datas.slice(0);


        var fields = ['amount', 'contributionType'];
        for (var i = 0; i < fields.length; i++) {
            $scope[fields[i]] = "";
        }

        var contribution = Contribution.getNewClassName();

        contribution.save(data, {
            success: function (contribution) {
                console.log('contribution: ' + contribution.id)
                data.id = contribution.id;
                data.date = $filter('date')(d, 'MM/dd/yyyy');
                temp.push(data);

                var sort = _.sortBy(temp, function (num) {
                    return new Date(num.date).valueOf();
                });
                $scope.datas = sort;
                $scope.$apply();
            },
            error: function (contribution, error) {
                // The save failed.
                // error is a Parse.Error with an error code and description.
                console.log('Error: ' + Error)
            }
        });
    };

    $scope.$on("edit", function (e, a) {
        var query = new Parse.Query(Contribution.getClassName());
        query.get(a.updateData.id, {
            success: function (contribution) {
                contribution.save(null, {
                    success: function (myObject) {
                        // The object was deleted from the Parse Cloud.
                        var d = new Date(a.updateData.date);

                        myObject.set("date", $filter('date')(d, 'yyyy-MM-dd'));
                        myObject.set("amount", a.updateData.amount);
                        myObject.set("type", a.updateData.type);
                        myObject.save();
                        console.log("update: " + myObject);
                    },
                    error: function (myObject, error) {
                        // The delete failed.
                        // error is a Parse.Error with an error code and description.
                        console.log("Error: " + error);
                    }
                });
            },
            error: function (object, error) {
                // The object was not retrieved successfully.
                // error is a Parse.Error with an error code and description.
                console.log("Error: " + error);
            }
        });
        console.log(a.updateData)
        $scope.datas = a.allData;
        $scope.$apply();
    });

    $scope.enable = 'No';
    $scope.edit = function () {
        console.log($scope.enable);
        ($scope.enable === 'No') ? $scope.enable = 'Yes' : $scope.enable = 'No';
    };

    $scope.signIn = function (email, password) {
        console.log('email: ' + email + ' password: ' + password);
    };

    $http.get('contributionType.json').success(function (data, status, headers, config) {
        $scope.contributions = data;
    });

})