var service = angular.module('ffc.service', []);


service.factory('parse', function() {
    var parseService;
    parseService = function() {
        //initialize Parse
        Parse.initialize("4UH7euehWFmYaXCBxNkG1YOj4ejadcuon6B8WTm3", "qQCDqbB1ekPEjEbEZm60se0bhJBcXvMvclvd3M0N");
        //Creates a new model with defined attributes.
        this.Contribution = Parse.Object.extend("contribution");
        
    };
    parseService.prototype.getClassName = function() {
        return this.Contribution;
    };
    parseService.prototype.getNewClassName = function() {
        return new this.Contribution();
    };
    parseService.prototype.getData = function(limit,callback) {
        //Parse.Query defines a query that is used to fetch Parse.Objects.
        var query = new Parse.Query(this.Contribution);
        //Sets the limit of the number of results to return.
        query.limit(limit);
        //Sorts the results in descending order by the given key.
        query.descending("date");
        //Retrieves a list of ParseObjects that satisfy this query.
        query.find(callback);
    };
    return parseService;
});

service.factory('contribution', function($filter) {
    var contributionService;
    
    var Contribution = (function(){
        var Contribution = function(obj) {
            this.id = obj.id;
            this.date = obj.date;
            this.amount = obj.amount;
            this.type = obj.type;
        };
        Contribution.prototype.setDate = function() {
            var d = new Date(this.date);
            d.setDate(d.getDate()+1);
            var dt = $filter('date')(d, 'MM/dd/yyyy');
            this.date = dt;
        }
        return Contribution;
    })();
    
    contributionService = function(results) {
        var data = [];
        //loops through the results set
        for (var i = 0; i < results.length; i++) {
            //creates a new contribution object
            var contribution = new Contribution({
                id: results[i].id,
                date: results[i].get('date'),
                amount: results[i].get('amount'),
                type: results[i].get('type'),

            });
            //corrects the date
            contribution.setDate();
            //pushes the contribution in the data array
            data.push(contribution);
        }
        return data;
    };

    return contributionService;
});