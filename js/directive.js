var app = angular.module('ffc.directives', []);

app.directive('popOver', function ($compile) {
    return {
        link: function (scope, iElement, iAttrs, ctrl) {

            iElement.popover({
                html: true,
                content: function () {
                    var content = "";

                    content = '<form class="form-horizontal">' + '<div class="control-group">' + '<input type="text" id="inputEmail" ng-model="email" placeholder="Enter Email Address" ui-date>' + '</div>' + '<div class="control-group">' + '<input type="password" id="inputPassword" ng-model="password" placeholder="Enter Password">' + '</div>' + '<button class="btn btn-block" ng-click="signIn(email,password)">Sign-In</button>' + '</form>'
                    return content;
                }
            });

        }
    };

});
app.directive('contenteditable', function () {
    return {
        require: 'ngModel',
        link: function (scope, iElement, iAttrs, ctrl) {
            iElement.blur(function () {
                var newValue;
                var arr = _.map(scope.datas, function (value, key, list) {
                    if (iAttrs.ident === value.id) {
                        var amount = iElement.parent()[0].children[2].innerText;
                        var a = Number(amount.replace("$", "").replace(",", ""));
                        value = {
                            id: iAttrs.ident,
                            date: iElement.parent()[0].children[1].innerText,
                            amount: a.valueOf(),
                            type: iElement.parent()[0].children[3].innerText
                        };
                        newValue = value;

                    }
                    return value;
                });
                scope.$emit("edit", {
                    allData: arr,
                    updateData: newValue
                });
            });
        }
    };
});

app.directive('highChart', function () {
    return {
        restrict: 'E',
        link: function (scope, iElement, iAttrs) {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: iAttrs.id,
                    type: 'column',
                    backgroundColor: 'none'
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'FFC Tithe and Offerings'
                },
                subtitle: {
                    text: 'Contribution'
                },
                xAxis: {
                    categories: _.pluck(scope.datas, "date"),
                    labels: {

                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'FFC Tithe and Offerings'
                    }
                },
                legend: {
                    backgroundColor: '#FFFFFF',
                    floating: false,
                    shadow: true
                },
                tooltip: {
                    formatter: function () {
                        return '' + this.x + ': $' + Highcharts.numberFormat(this.y, 2);
                    }
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '$' + Highcharts.numberFormat(this.y, 2);
                            }
                        }
                    }
                },
                series: [{
                        name: 'FFC Tithe and Offerings',
                        data: _.pluck(scope.datas, 'amount')

                }]
            });

            scope.$watch('datas', function (newValue, oldValue) {
                chart.series[0].setData(_.pluck(newValue, 'amount'))
                chart.xAxis[0].setCategories(_.pluck(newValue, 'date'))
            });
        }
    };
});


app.directive("modal", function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/modal.html',
        link: function (scope, iElement, iAttrs) {
            scope.idname = iAttrs.idname;
        }
    }

})